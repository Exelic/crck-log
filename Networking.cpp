#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include "Networking.hpp"

bool Networking::PostRequest(string host, string logs) {
    string PHP_File = "recvlogs.php";
    int portno =        80;
    string request = "POST /" + PHP_File + "?logs=" + logs + " HTTP/1.1\r\nHOST: " + host + "\r\n\r\n"; //pretty sure this is valid, but check it just to be sure

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;
    char response[4096];

    //creating socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        Networking::error("ERROR opening socket");
        return 0;
    }

    //resolving ip address
    server = gethostbyname(host.c_str());
    if (server == NULL) {
        Networking::error("ERROR, no such host");
        return 0;
    }


    //fill in the struct
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    //connect to socket
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
        Networking::error("ERROR connecting");
        return 0;
    }
    //send the request
    total = strlen(request.c_str());
    sent = 0;
    do {
        bytes = write(sockfd,request.c_str()+sent,total-sent);
        if (bytes < 0) {
            Networking::error("ERROR writing message to socket");
            return 0;
        }

        if (bytes == 0)
            break;
        sent+=bytes;
    } while (sent < total);

    //receive the response
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0) {
            Networking::error("ERROR reading response from socket");
            return 0;
        }
        if (bytes == 0)
            break;
        received+=bytes;
    } while (received < total);

    if (received == total) {
        Networking::error("ERROR storing complete response from socket");
        return 0;
    }
    /* close the socket */
    close(sockfd);

    return 1;
}