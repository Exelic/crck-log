#ifndef TEST_NETWORKING_HPP
#define TEST_NETWORKING_HPP

#include <iostream>

using namespace std;
class Networking {
public:
    static bool PostRequest(string host, string logs);
    static void error(const char *msg) {
        cout << "Post request error: " << msg << endl;
    }
};
#endif //TEST_NETWORKING_HPP
